#include<iostream>
#include <ctime>

template<typename T>

class Stack
{
public:
	Stack() : sizeArr(0), arr{}, lastIndex(0)
	{};

	Stack(int _sizeArr) : sizeArr(_sizeArr), arr(new T[_sizeArr + 1]), lastIndex(_sizeArr - 1)
	{};

	~Stack()
	{
		delete[] arr; 
	};


	void FillArr()
	{
		std::srand(unsigned(std::time(0)));

		for (int i = 0; i < sizeArr; i++)
		{
			arr[i] = std::rand() % 10;
			std::cout << arr[i] << ' ';
			lastIndex = i;
		};
		std::cout << '\n';
	};

	void GetArr()
	{
		for (int i = 0; i < lastIndex + 1; i++)
		{
			std::cout << arr[i] << ' ';
		};
		std::cout << '\n';
	};

	void push(T first = 0)
	{
		
		if (lastIndex > sizeArr - 1)
		{

			T* temp_array = new T[sizeArr * 2];
			for (int i = 0; i < lastIndex + 1; ++i)
			{
				temp_array[i] = arr[i];
			}
			delete[] arr;
			arr = temp_array;
		};

		
		arr[lastIndex + 1] = first;
		lastIndex++;

		GetArr();
		
	};

	void pop()
	{
		if (lastIndex > 0)
		{
			lastIndex--;
		}
		
		GetArr();
	};


private :
	int sizeArr;
	T* arr;
	int lastIndex;
};

int main()
{
	Stack<int> someArray(5);
	someArray.FillArr();
	someArray.GetArr();
	someArray.push(7);
	someArray.push(7);
	someArray.push(7);
	someArray.pop();
	someArray.pop();
	someArray.pop();
	someArray.push(7);

	return 0;
};